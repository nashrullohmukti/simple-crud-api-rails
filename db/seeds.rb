# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

for a in 1..100000
    User.create(firstname: Faker::Name.first_name, lastname: Faker::Name.last_name, username: Faker::Internet.username, phone_number: Faker::PhoneNumber.phone_number, email: Faker::Internet.email, address: Faker::Address.full_address)
end