class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :firstname, null: false
      t.string :lastname
      t.string :username
      t.string :phone_number
      t.string :email, null: false
      t.string :address
      t.timestamps null: false
    end
  end
end
