class ApplicationController < ActionController::API
    include ActionController::Caching
    before_action :set_default_response_format

    private
        def set_default_response_format
            request.format = :json
        end

        def bad_request
            render json: {
                message: "Bad Request!",
            }, status: :bad_request
        end

        def record_not_found
            render json: {
                message: "Data not found!",
            }, status: :not_found
        end

        def unprocessable_entity(errors)
            render json: {
                message: "Failed!",
                errors: errors,
            }, status: :unprocessable_entity
        end
end
