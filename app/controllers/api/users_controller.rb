class Api::UsersController < ApplicationController
    before_action :find_user, only: [:update, :show, :destroy]
    rescue_from ActionController::ParameterMissing, with: :bad_request
    
    def index
        @user = User.all.to_a
    end

    def show
        if !@user.present?
            self.record_not_found
        end
    end

    def create
        @user = User.new(user_params)
        if !@user.save
            self.unprocessable_entity(@user.errors.full_messages)
        end    
    end

    def update
        if !@user.present?
            self.record_not_found
        end

        if !@user.update(user_params)
            self.unprocessable_entity(@user.errors.full_messages)
        end
    end

    def destroy
        if @user.present?
            @user.destroy
        else
            self.record_not_found
        end
    end

    private
        def user_params
            params.require(:user).permit(:firstname, :lastname, :username, :email, :phone_number, :address)
        end

        def find_user
            @user = User.find(params[:id])
        end

        def record_not_found
            super
        end

        def bad_request
            super
        end

        def unprocessable_entity(errors)
            super
        end
    end
