json.message "Success!"

json.data do
    json.array! @user do |user|
        json.id user.id
        json.firstname user.firstname
        json.lastname user.lastname
        json.username user.username
        json.phone_number user.phone_number
        json.email user.email
        json.address user.address
    end
end
